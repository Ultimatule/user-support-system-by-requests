<?php

namespace Database\Seeders;

use App\Models\Request;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class RequestsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Request::create([
            'name' => 'John Smith',
            'email' => 'johnsmithtest@gmail.com',
            'status' => 'Active',
            'message' => 'lorem Ipsum is simply dummy',
        ]);
        Request::create([
            'name' => 'Karen Smith',
            'email' => 'karensmithtest@gmail.com',
            'status' => 'Resolved',
            'message' => 'lorem Ipsum is simply dummy and therefore',
            'comment' => 'lorem Ipsum is resolved'
        ]);
        Request::create([
            'name' => 'Dutch Doe',
            'email' => 'dutchdoetest@gmail.com',
            'status' => 'Active',
            'message' => 'Some request to resolve',
        ]);
        Request::create([
            'name' => 'Jane Doe',
            'email' => 'janedoetest@gmail.com',
            'status' => 'Resolved',
            'message' => 'Some request to resolve with lot of problems',
            'comment' => 'Request is resolved'
        ]);
    }
}
