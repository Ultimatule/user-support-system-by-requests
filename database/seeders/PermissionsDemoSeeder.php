<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use Spatie\Permission\PermissionRegistrar;

class PermissionsDemoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        app()[PermissionRegistrar::class]->forgetCachedPermissions();

        Permission::create(['name' => 'resolve requests']);

        $roleSupervisor = Role::create(['name' => 'supervisor']);
        $roleSupervisor->givePermissionTo('resolve requests');

        $user = User::factory()->create([
            'name' => 'Test supervisor',
            'email' => 'testtest@test.com',
            'password' => bcrypt('12345678')
        ]);
        $user->assignRole($roleSupervisor);
    }
}
