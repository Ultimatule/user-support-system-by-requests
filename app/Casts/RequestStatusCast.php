<?php

namespace App\Casts;

use App\Enums\RequestStatus;
use Illuminate\Contracts\Database\Eloquent\CastsAttributes;

class RequestStatusCast implements CastsAttributes
{

    public function get($model, string $key, $value, array $attributes)
    {
        return $value;
    }

    public function set($model, string $key, $value, array $attributes)
    {
        if (!RequestStatus::isValidStatus($value)) {
            throw new \InvalidArgumentException('Invalid status');
        }
        return $value;
    }
}
