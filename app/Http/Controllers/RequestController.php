<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreRequestRequest;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request as HttpRequest;
use App\Models\Request;

class RequestController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth:sanctum', 'permission:resolve requests'])->only('index', 'update');
    }
    public function index(HttpRequest $request)
    {
        $requests = Request::query();

        $requests
            ->when($request->query('status'), function (Builder $query, string $status) {
                return $query->where('status', $status);
            })
            ->when($request->query('date_from'), function (Builder $query, string $dateFrom) {
                return $query->whereDate('created_at', '>=', $dateFrom);
            })
            ->when($request->query('date_to'), function (Builder $query, string $dateFrom) {
                return $query->whereDate('created_at', '>=', $dateFrom);
            });

        return response()->json($requests->get());
    }

    public function store(StoreRequestRequest $request)
    {
        $newRequest = Request::create($request->validated());

        return response()->json($newRequest, 201);
    }

    public function update(HttpRequest $request, string $id)
    {
        $validatedData = $request->validate([
            'status' => 'required|in:Active,Resolved',
            'comment' => 'required_if:status,Resolved|string'
        ]);

        $requestToUpdate = Request::find($id);

        if (!$requestToUpdate) {
            return response()->json(['error' => 'Request not found'], 404);
        }

        $requestToUpdate->update($validatedData);

        return response()->json($requestToUpdate);
    }
}
