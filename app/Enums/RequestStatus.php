<?php

namespace App\Enums;

final class RequestStatus
{
    const Active = 'Active';
    const Resolved = 'Resolved';

    private function __construct()
    {
    }

    public static function getStatuses(): array
    {
        return [
            self::Active,
            self::Resolved,
        ];
    }

    public static function isValidStatus(string $status): bool
    {
        return in_array($status, self::getStatuses());
    }
}
