<?php

namespace App\Models;

use App\Casts\RequestStatusCast;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @method static \Illuminate\Database\Eloquent\Builder|static create(array $attributes = [])
 * @method static find(string $id)
 */
class Request extends Model
{
    use HasFactory;

    protected $table='requests';

    protected $fillable=[
        'name',
        'email',
        'status',
        'message',
        'comment'
    ];

    protected $casts = [
        'status' => RequestStatusCast::class,
    ];
}
