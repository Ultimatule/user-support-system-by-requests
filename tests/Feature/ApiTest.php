<?php

namespace Tests\Feature;

use App\Models\User;
use Spatie\Permission\Models\Role;
use Spatie\Permission\PermissionRegistrar;
use Tests\TestCase;

class ApiTest extends TestCase
{

    /** @test */
    public function it_can_get_requests()
    {
        app()[PermissionRegistrar::class]->forgetCachedPermissions();

        $roleSupervisor = Role::findOrCreate('supervisor');
        $roleSupervisor->givePermissionTo('resolve requests');

        $userPassword = '12345678';
        $user = User::firstOrCreate(
            ['email' => 'testtest@test.com'],
            ['name' => 'Test supervisor', 'password' => bcrypt($userPassword)]
        );
        $user->assignRole($roleSupervisor);

        $response = $this->post('/api/login', [
            'email' => $user->email,
            'password' => $userPassword,
        ]);

        $token = $response->json('token');

        $response = $this->withHeader('Authorization', 'Bearer ' . $token)
            ->getJson('/api/requests');

        $response->assertStatus(200);
    }
}
