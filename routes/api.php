<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\RequestController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/requests', [RequestController::class, 'store'])->name('request.upload');
Route::post('/login', [AuthController::class, 'login']);
Route::middleware(['auth:sanctum', 'role_or_permission:resolve requests'])->group(function () {
    Route::get('/requests', [RequestController::class, 'index'])->name('request.get');
    Route::put('/requests/{request}', [RequestController::class, 'update'])->name('request.update');
});
